# Repository for IT Security education 2021 Baseline Project Group 3  

[Dansk/Danish](https://its2021team3a.gitlab.io/its_2021_baseline_grp3/index-dk.html)

## Group members:  
- John Richard Conley
- Levente Atilla Taner  
- Marc Rasmus Jensen  
- Martin Hansson  
- Thomas Tønnes Edelberth    

## Overview
The aim of this project is to create a virtualized environment consisting of several devices with different operating systems, emulating a real world scenario to better understand how similar systems are set up from scratch, and how they work. This will provide experience of working with systems with security in mind and an environment for future tests.  


## System to be built:
<table>
	<tr>
		<th>Device name</th>
		<th>Description</th>
		<th>OS</th>
		<th>Disk</th>
		<th>RAM</th>
		<th>CPU</th>
	</tr>
	<tr>
		<td>[Dc1](setup/Dc1.md)</td>
		<td>Primary domain-controller and DHCP</td>
		<td>WS 2016</td>
		<td>20 GB</td>
		<td>4 GB</td>
		<td>2</td>
	</tr>
	<tr>
		<td>[Dc2](setup/Dc2.md)</td>
		<td>Secondary domain controller</td>
		<td>WS 2016</td>
		<td>20 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[Fil1](setup/Fil1.md)</td>
		<td>File server</td>
		<td>WS 2016</td>
		<td>20 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>Backup</td>
		<td>Backup storage server</td>
		<td>WS 2016</td>
		<td>40 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[Sql](setup/Sql.md)</td>
		<td>MS SQL server 2016</td>
		<td>WS 2016</td>
		<td>40 GB</td>
		<td>8 GB</td>
		<td>2</td>
	</tr>
	<tr>
		<td>[Web A](setup/Web-A.md)</td>
		<td>Nginx web server</td>
		<td>Linux</td>
		<td>16 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[Web B](setup/Web-B.md)</td>
		<td>Nginx web server</td>
		<td>Linux</td>
		<td>16 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[Win1](setup/Win1.md)</td>
		<td>Domain PC</td>
		<td>Windows 10</td>
		<td>20 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[Win2](setup/Win2.md)</td>
		<td>Domain PC</td>
		<td>Windows 10</td>
		<td>20 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[Kali](setup/Kali.md)</td>
		<td>Tool</td>
		<td>Kali Linux</td>
		<td>16 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[LibreNMS](setup/LibreNMS.md)</td>
		<td>Network surveillance SNMP</td>
		<td>Linux</td>
		<td>16 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[vSRX](setup/vSRX.md)</td>
		<td>Router/firewall</td>
		<td>WS 2016</td>
		<td>16 GB</td>
		<td>4 GB</td>
		<td>2</td>
	</tr>
</table>

This environment will be running on a server running VMWare ESXi.   
Server details: 3 Dell servers with 96 GB RAM 2 Intel (R) Xeon (R) Silver with 8 cores in each, 4 network cards 1 GB and 1 storage connectors which are connected to storage controllers A and B via 12GB connection to a common direct access storage which makes vmotion possible.

## IP addresses  
The group has been assigned VLAN103 and the IP range 10.56.35.1/24  
Additionally the network has been divided into four segments:  
DMZ:  
.0 -> .63

Servere:  
.64 -> .127  

Tools:  
.128 -> .191  

DHCP:  
.192 -> .255  

## High Level Diagram  

![diagram](diagram.png)



