# Sql  

Sql alias SQL Server  

## Prerequisites  
- Connection to VMWare ESXi (see [hypervisor guide](Hypervisor.md))  
- Windows 2016 Server ISO file (provided by lecturers, otherwise see [here](https://www.microsoft.com/en-us/evalcenter/evaluate-windows-server-2016))  
- Working Primary Domain Controller (see [Dc1](Dc1.md))  

## Install a Windows Server 2016 VM  
- Go through the Windows Server guided installation (select desktop experience and custom install)    
- The settings used for this installation are:  
![Sqlsettings](pics/Sqlsettings.png)  
- Remember to have it connected to the network!  
- Install VMWare Tools  

## Set up server roles
- Set dynamic IP, gateway, and DNS server  
- Install server roles from server manager > manage > add roles and features: SQL Server
- Configure SQL server  
