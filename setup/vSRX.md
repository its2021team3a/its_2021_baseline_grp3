# vSRX  

vSRX alias Juniper Virtual Firewall      

## Prerequisites  
- Connection to VMWare ESXi (see [hypervisor guide](Hypervisor.md))  
- vSRX installation file (provided by lecturers, otherwise see [here](https://www.juniper.net/us/en/dm/free-vsrx-trial/))  

## Setup a vSRX VM   

- The settings used for this installation are:    
![vSRXsettings](pics/vSRXsettings.png)  
- Please note that interface 1 is not connected!!  
- Remember to have interface 2 and 3 connected to the network!  
- Configuration file can be seen [here](https://its2021team3a.gitlab.io/its_2021_baseline_grp3/setup/confs/vSRX.conf).  
- Manual configuration:  
To enter Juniper cli:  
```
cli
```
After, to enter configuration mode  
```
configure
```
or  
```
edit
```

1. Set root password  
```
set system root-authentication plain-text-password

```
2. Set hostname  
```
set system host-name vSRX01

```
3. Set DNS  
```
set system name-server 10.56.35.66

```
4. Enable SSH  
```
set system services ssh
set system services ssh root-login allow

```
4.1 Optional add user  

```
set system login user bruger uid 2001
set system login user bruger class super-user
set system login user bruger authentication plain-text-password
set system login user bruger authentication ssh-rsa PUBLIC KEY HERE
```
5. Configure IP addresses on interfaces  
```
set interfaces  lo0 unit 0 family inet address 192.168.255.3/32
set interfaces  ge-0/0/0 unit 0 family inet address 10.56.35.70/24
set interfaces  ge-0/0/1 unit 0 family inet address 10.56.62.1/24
```
6. Zone configuration: Open Firewall  
```
set security zones security-zone vlan103 interfaces ge-0/0/0.0
set security zones security-zone vlan203 interfaces ge-0/0/1.0
``` 
- vlan 203 to vlan103  
```
set security policies  from-zone vlan203 to-zone vlan103 policy default-permit match source-address any
set security policies  from-zone vlan203 to-zone vlan103 policy default-permit match destination-address any
set security policies  from-zone vlan203 to-zone vlan103 policy default-permit match application junos-http
set security policies  from-zone vlan203 to-zone vlan103 policy default-permit match application junos-https
set security policies  from-zone vlan203 to-zone vlan103 policy default-permit match application junos-ping
set security policies  from-zone vlan203 to-zone vlan103 policy default-permit match application junos-dns-udp
set security policies  from-zone vlan203 to-zone vlan103 policy default-permit match application junos-dns-tcp
set security policies  from-zone vlan203 to-zone vlan103 policy default-permit match application junos-dhcp-client
set security policies  from-zone vlan203 to-zone vlan103 policy default-permit then permit
```
- vlan 103 to vlan203  
```
set security policies  from-zone vlan103 to-zone vlan203 policy default-permit match source-address any
set security policies  from-zone vlan103 to-zone vlan203 policy default-permit match destination-address any
set security policies  from-zone vlan103 to-zone vlan203 policy default-permit match application junos-http
set security policies  from-zone vlan103 to-zone vlan203 policy default-permit match application junos-https
set security policies  from-zone vlan103 to-zone vlan203 policy default-permit match application junos-ping
set security policies  from-zone vlan103 to-zone vlan203 policy default-permit match application junos-dns-udp
set security policies  from-zone vlan103 to-zone vlan203 policy default-permit match application junos-dns-tcp
set security policies  from-zone vlan103 to-zone vlan203 policy default-permit match application junos-dhcp-server
set security policies  from-zone vlan103 to-zone vlan203 policy default-permit then permit
```
7. Configure OSPF  
```
set protocols ospf area 0.0.0.0 interface ge-0/0/0.0
set protocols ospf area 0.0.0.0 interface ge-0/0/1.0
set protocols ospf area 0.0.0.0 interface lo0.0 passive
set security zones security-zone vlan103 interfaces ge-0/0/0.0 host-inbound-traffic protocols ospf
set security zones security-zone vlan203 interfaces ge-0/0/1.0 host-inbound-traffic protocols ospf
```
8. Extra: GUI Web Management Interface  
```
set system services web-management http interface ge-0/0/0.0
```
Lastly  
```
commit
exit
```
