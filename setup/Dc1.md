# Dc1  

Dc1 alias Domain Controller 1, primary domain controller  

## Prerequisites  
- Connection to VMWare ESXi (see [hypervisor guide](Hypervisor.md))  
- Windows 2016 Server ISO file (provided by lecturers, otherwise see [here](https://www.microsoft.com/en-us/evalcenter/evaluate-windows-server-2016))

## Install a Windows Server 2016 VM  
- Go through the Windows Server guided installation (select desktop experience and custom install)    
- The settings used for this installation are:  
![Dc1settings](pics/Dc1settings.png)  
- Remember to have it connected to the network!  
- Install VMWare Tools  

## Set up server roles
- Set static IP, gateway, and DNS server  
- Install server roles from server manager > manage > add roles and features: DHCP, DNS, AD DS, SNMP  
- Configure DHCP scope  
- Configure DNS   
