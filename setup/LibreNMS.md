# LibreNMS  

LibreNMS alias Linux SNMP server    

## Prerequisites  
- Connection to VMWare ESXi (see [hypervisor guide](Hypervisor.md))  
- Preferred Linux distribution ISO file (this project uses [Debian 10 buster](https://www.debian.org/))
- Other VMs that have SNMP installed and running
- Optional: SSH-keypair and SSH client  

## Install a Debian 10 VM  
- Go through the Debian 10 guided installation (only install standard system utilities at software selection step, others are not needed)  
- The settings used for this VM are:  
![LibreNMSsettings](pics/LibreNMSsettings.png)  
- Remember to have it connected to the network!  
- Install VMWare Tools   
- Install updates using: apt update && apt upgrade -y  

## Install LibreNMS   
- The official installation guide can be seen [here](https://docs.librenms.org/Installation/Install-LibreNMS/).  

## Automatic scripted setup  
- Note: The script has not been tested in a real environment. It is recommended to double-check the files according to the documentation after the script is done.  ` 
- Prerequisites for install script:  
  	- Gitlab user with SSH public key added  
	- local user on server  
	- Static IP address in mind, known gateway, netmask, community for snmp  
	- SSH port wishes   
- If you want to use an interactive automated script for setting up the LibreNMS server - you can use [this file](https://its2021team3a.gitlab.io/its_2021_baseline_grp3/setup/scripts/librenmsscript.sh).  
- To download the file to your webserver:  
	- apt install -y curl  
	- curl -O https://its2021team3a.gitlab.io/its_2021_baseline_grp3/setup/scripts/librenmsscript.sh  
- To run the script:  
	- chmod +x librenmsscript.sh  
	- ./librenmsscript.sh  
	- answer the interactive prompts   
	- reboot machine after the script is finished  
	
- Go to to the IP address of the LibreNMS server on another machine using the webbrowser. (Example: http://librenms.example.com/install)   
- After completing the web installer, run "chown librenms:librenms /opt/librenms/config.php"   

## Add other SNMP devices to LibreNMS  
- Add devices through the web interface. (Example: http://librenms.example.com/addhost)  
