#!/bin/bash

getvars(){
echo What should the static address be? Example: 10.56.35.160
read IPADDRESS
echo What should the netmask be? Example: 255.255.255.0
read NETMASK
echo What should the gateway be? Example: 10.56.35.1
read GATEWAYIP
echo What should the SSH port be? Example: 3333
read SSHPORT
echo Enter a local username. Example: bruger
read LOCALUSER
echo Enter GitLab username. Example: jambove
read GITLABUSER
INTERFACE=$(ip r | grep default | awk '{print $5}')
echo $INTERFACE
echo Enter a community for the SNMP. Example: Community
read COMMUNITY
}
setvars(){
IPADDRESS=10.56.35.160
NETMASK=255.255.255.0
GATEWAYIP=10.56.35.1
SSHPORT=3333
LOCALUSER=bruger
GITLABUSER=jambove
COMMUNITY=community
}
setstaticip(){
cat > /etc/network/interfaces.d/staticip-$INTERFACE <<EOF
  allow-hotplug $INTERFACE
  iface $INTERFACE inet static
      address $IPADDRESS 
      netmask $NETMASK
      gateway $GATEWAYIP
EOF
sed 's/.*'$INTERFACE' inet dhcp.*//' /etc/network/interfaces
}

updupg(){
apt-get update && apt-get upgrade -y
}

webserver(){
apt install -y nginx curl
systemctl enable nginx
systemctl start nginx
curl $IPADDRESS
}

sshserver(){
apt install -y ssh openssh-server 
cat >> /etc/ssh/sshd_config<<EOF
Port $SSHPORT
PasswordAuthentication no
PermitRootLogin no
EOF
curl http://gitlab.com/$GITLABUSER.keys >> /home/$LOCALUSER/.ssh/authorized_keys
systemctl enable ssh
systemctl start ssh
}  

firewalliptables(){
echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc
source /root/.bashrc
iptables -t filter -A INPUT -i lo -j ACCEPT
iptables -t filter -A OUTPUT -o lo -j ACCEPT
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --dport $SSHPORT -j ACCEPT
iptables -A OUTPUT -p tcp --dport $SSHPORT -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -t filter -P INPUT DROP
iptables -t filter -P FORWARD DROP
iptables -t filter -P OUTPUT DROP
iptables-save > /etc/iptables/rules.v4
ip6tables-save > /etc/iptables/rules.v6
cat >> /etc/crontab<<EOF
@reboot /sbin/iptables-restore < /etc/iptables/rules.v4
@reboot /sbin/ip6tables-restore < /etc/iptables/rules.v6
EOF
}

firewallufw(){
apt install -y ufw
echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc
source /root/.bashrc
ufw allow $SSHPORT
ufw allow 80
ufw default deny incoming
ufw enable
}

librenmssetup(){
apt install -y acl curl composer fping git graphviz imagemagick mariadb-client mariadb-server mtr-tiny nginx-full nmap php7.3-cli php7.3-curl php7.3-fpm php7.3-gd php7.3-json php7.3-mbstring php7.3-mysql php7.3-snmp php7.3-xml php7.3-zip python3-dotenv python3-pymysql python3-redis python3-setuptools python3-systemd rrdtool snmp snmpd whois sudo
useradd librenms -d /opt/librenms -M -r -s "$(which bash)"
cd /opt
git clone https://github.com/librenms/librenms.git
chown -R librenms:librenms /opt/librenms
chmod 771 /opt/librenms
setfacl -d -m g::rwx /opt/librenms/rrd /opt/librenms/logs /opt/librenms/bootstrap/cache/ /opt/librenms/storage/
setfacl -R -m g::rwx /opt/librenms/rrd /opt/librenms/logs /opt/librenms/bootstrap/cache/ /opt/librenms/storage/
sudo su - librenms << EOF
./scripts/composer_wrapper.php install --no-dev
EOF
wget https://getcomposer.org/composer-stable.phar
mv composer-stable.phar /usr/bin/composer
chmod +x /usr/bin/composer
sed -i 's/date\.timezone\ =\ .*/date\.timezone\ =\ Europe\/UTC/g' /etc/php/7.3/fpm/php.ini
sed -i 's/date\.timezone\ =\ .*/date\.timezone\ =\ Europe\/UTC/g' /etc/php/7.3/cli/php.ini
timedatectl set-timezone Etc/UTC
sed -i '/\[mysqld\]/a \
innodb_file_per_table=1\
lower_case_table_names=0' /etc/mysql/mariadb.conf.d/50-server.cnf
mysql -u root << EOF
CREATE DATABASE librenms CHARACTER SET utf8 COLLATE utf8_unicode_ci;
CREATE USER 'librenms'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON librenms.* TO 'librenms'@'localhost';
FLUSH PRIVILEGES;
EOF
cp /etc/php/7.3/fpm/pool.d/www.conf /etc/php/7.3/fpm/pool.d/librenms.conf
sed -i 's/\[www\]/\[librenms\]/g' /etc/php/7.3/fpm/pool.d/librenms.conf
sed -i 's/user\ \=\ .*/user\ \=\ librenms/g' /etc/php/7.3/fpm/pool.d/librenms.conf
sed -i 's/group\ \=\ .*/group\ \=\ librenms/g' /etc/php/7.3/fpm/pool.d/librenms.conf
sed -i 's/listen\ \=\ .*/listen\ \=\ \/run\/php-fpm-librenms\.sock/g' /etc/php/7.3/fpm/pool.d/librenms.conf
cat > /etc/nginx/sites-enabled/librenms.vhost<<EOF
server {
 listen      80;
 server_name $IPADDRESS;
 root        /opt/librenms/html;
 index       index.php;

 charset utf-8;
 gzip on;
 gzip_types text/css application/javascript text/javascript application/x-javascript image/svg+xml text/plain text/xsd text/xsl text/xml image/x-icon;
 location / {
  try_files $uri $uri/ /index.php?$query_string;
 }
 location ~ [^/]\.php(/|$) {
  fastcgi_pass unix:/run/php-fpm-librenms.sock;
  fastcgi_split_path_info ^(.+\.php)(/.+)$;
  include fastcgi.conf;
 }
 location ~ /\.(?!well-known).* {
  deny all;
 }
}
EOF
rm /etc/nginx/sites-enabled/default
systemctl reload nginx
systemctl restart php7.3-fpm
ln -s /opt/librenms/lnms /usr/bin/lnms
cp /opt/librenms/misc/lnms-completion.bash /etc/bash_completion.d/
cp /opt/librenms/snmpd.conf.example /etc/snmp/snmpd.conf
sed -i 's/RANDOMSTRINGGOESHERE/$COMMUNITY/g' /etc/snmp/snmpd.conf
curl -o /usr/bin/distro https://raw.githubusercontent.com/librenms/librenms-agent/master/snmp/distro
chmod +x /usr/bin/distro
systemctl enable snmpd
systemctl restart snmpd
cp /opt/librenms/librenms.nonroot.cron /etc/cron.d/librenms
}



checkup(){
echo CHECKUPS:
echo " "
echo Static ip:
cat /etc/network/interfaces.d/staticip-$INTERFACE
echo " "
echo Web server status:
ps -aux | grep "[n]ginx"
echo " "
echo SSH server status:
ps -aux | grep "[s]sh"
echo " "
echo Firewall status:
ufw status
}

main(){
getvars
setstaticip
updupg
webserver
sshserver
#firewalliptables
#firewallufw
#checkup
librenmssetup
}

main
