# Kali  

Kali is a Linux distribution built for penetration testing  

## Prerequisites  
- Connection to VMWare ESXi (see [hypervisor guide](Hypervisor.md))  
- Kali Linux distribution [ISO file](https://www.kali.org/)
- Optional: SSH-keypair and SSH client  
- Note: all commands are expected to be run as root or a privileged user

## Install a Kali VM  
- Go through the Kali guided installation  
- The settings used for this VM are:  
![Kalisettings](pics/Kalisettings.png)  
- Remember to have it connected to the network!  
- Install VMWare Tools   
- Install updates using: apt update && apt upgrade -y  

## Set static ip  
![Kaliinterfaces](pics/Kaliinterfaces.png)  
- Right click the network icon on the top right corner panel  
- Click "Edit connections"  
- Select "Wired connection 1"  
- Go to the IPv4 Settings tab  
- Select Manual method  
- Write the static IP address in the Adress column: 10.56.35.150  
- Write the netmask in the Netmask column: 24  
- Write the default gateway in the Gateway column: 10.56.35.1  

## Install, harden/configure, and enable SSH server  
SSH is the best way to securely manage servers remotely.  
- apt install ssh openssh-server #install SSH and SSH server (best way to securely manage servers)  
- using your preferred text editor (for example nano, vi, vim, emacs, etc.) edit /etc/ssh/sshd_config  
- add the lines   
```'
Port 3333 ## Change default SSH port 22 to 3333 - not for security but to avoid automated bot scans
PasswordAuthentication no ## Disables password authentication for security, SSH key authentication is more secure
PermitRootLogin no ## Disables root login using SSH for security, SSHing into user then su is more secure
```
- place your [public SSH keys](https://docs.gitlab.com/ee/ssh/#generate-an-ssh-key-pair) into /home/bruger/.ssh/authorized_keys  (an easy way to do it is using curl http://gitlab.com/user.keys >> /home/bruger/.ssh/authorized_keys)   
- systemctl enable ssh  #enable ssh server (sshd which means ssh daemon is symlinked to ssh) using systemctl that handles systemd services   
- systemctl start ssh #start ssh server right now using systemctl that handles systemd services  
- access your machine using "ssh bruger@10.56.35.56 -p3333"  
  
## Optional: Set firewall rules (it may interfere with the penetration testing)  
There are two approaches:  
  
### Using Iptables  
- echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc && source /root/.bashrc # add /usr/sbin/ to the PATH environmental variable so the system knows where to execute iptables from  
- iptables -t filter -A INPUT -i lo -j ACCEPT # allow loopback traffic (good practice, it is from your own machine)   
- iptables -t filter -A OUTPUT -o lo -j ACCEPT # allow loopback traffic (good practice, it is from your own machine)   
- iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT ## to make it work with established connections  
- iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT ## to make it work wit hestablishd connections  
- iptables -A INPUT -p tcp --dport 3333 -j ACCEPT # allow port 3333 (our SSH port) incoming traffic  
- iptables -A OUTPUT -p tcp --dport 3333 -j ACCEPT # allow port 3333 (our SSH port) outgoing traffic   
- iptables -t filter -P INPUT DROP  # drop input packets   
- iptables -t filter -P FORWARD DROP # drop forward packets    
- iptables -t filter -P OUTPUT DROP # drop output packets  
- iptables-save > /etc/iptables/rules.v4 # save configuration      
- ip6tables-save > /etc/iptables/rules.v6 # save ipv6 configuration   
- to restore rules after reboot, add the following line to your cron (crontab -e):  
```
@reboot /sbin/iptables-restore < /etc/iptables/rules.v4
@reboot /sbin/ip6tables-restore < /etc/iptables/rules.v6

```

### Using UFW (Uncomplicated FireWall - Iptables frontend/wrapper)  
- apt install ufw  #install ufw   
- echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc && source /root/.bashrc # add /usr/sbin/ to the PATH environmental variable so the system knows where to execute ufw from  
- ufw allow 3333  # allow port 3333 (our SSH port) incoming traffic  
- sudo ufw default deny incoming # deny incoming connections by default   
- ufw enable  # enable ufw iptables frontend firewall  
