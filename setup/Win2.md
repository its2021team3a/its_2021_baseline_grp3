# Win2  

Windows 10 - number 2   

## Prerequisites  
- Connection to VMWare ESXi (see [hypervisor guide](Hypervisor.md))  
- Windows 10 ISO file (provided by lecturers, otherwise see [here](https://www.microsoft.com/da-dk/software-download/windows10))
- Working Domain Controller (see [Dc1](Dc1.md))  
- Working vSRX (see [vSRX](vSRX.md))   

## Install a Windows 10 Pro VM  
- Go through the Windows 10 guided installation (select custom install)    
- The settings used for this installation are:  
![Win2settings](pics/Win2settings.png)  
- Remember to have it connected to the network!  
- Install VMWare Tools  

## Post-installation
- Use DHCP  
- Join the domain of the domain controller (Open file explorer, right click This PC and select properties, click advanced system settings on the left, select Computer Name on the top, and press change at the domain and workgroup settings)  
