# Web-A  

Web-A alias Linux webserver using nginx  

## Prerequisites  
- Connection to VMWare ESXi (see [hypervisor guide](Hypervisor.md))  
- Preferred Linux distribution ISO file (this project uses [Debian 10 buster](https://www.debian.org/))
- Optional: SSH-keypair and SSH client  
- Note: all commands are expected to be run as root or a privileged user

## Install a Debian 10 VM  
- Go through the Debian 10 guided installation (only install standard system utilities at software selection step, others are not needed)  
- The settings used for this VM are:  
![Web-Asettings](pics/Web-Asettings.png)  
- Remember to have it connected to the network!  
- Install VMWare Tools   
- Install updates using: apt update && apt upgrade -y  

## Automatic scripted setup  
- Prerequisites for install script:  
  	- Gitlab user with SSH public key added  
	- local user on webserver  
	- Static IP address in mind, known gateway, netmask  
	- SSH port wishes   
- If you want to use an interactive automated script for setting up the webserver - you can use [this file](https://its2021team3a.gitlab.io/its_2021_baseline_grp3/setup/scripts/webserverscript.sh).  
- To download the file to your webserver:  
	- apt install -y curl  
	- curl -O https://its2021team3a.gitlab.io/its_2021_baseline_grp3/setup/scripts/webserverscript.sh  
- To run the script:  
	- chmod +x webserverscript.sh  
	- ./webserverscript.sh  
	- answer the interactive prompts   
	- reboot machine after the script is finished  
  
## Manual setup  
- For manual setup, follow the instructions below  
- Run the commands as root or privileged user  
- Read and run the commands carefully and correctly  

## Set static ip  
- check your network interfaces using the command "ip a"   
- edit /etc/network/interfaces with your preferred editor (nano, vi, vim, emacs, etc.)  
- add the following lines to the end of the file:  
  ```
  allow-hotplug ens32 ## ens32 is the interface, allow-hotplug starts the interface when a "hotplug" event is detected.
  iface ens32 inet static ## set interface ens32 to have a static ip with details below
      address 10.56.35.55 ## the static ip address for the interface 
      netmask 255.255.255.0 ## netmask
      gateway 10.56.35.1 ## gateway
  ```  
   
## Install and enable web server  
- apt install nginx #install nginx, our chosen web server (due to simplicity, stability, and security)   
- systemctl enable nginx  #enable nginx on startup using systemctl that handles systemd services   
- systemctl start nginx   #start nginx right now using systemctl that handles systemd services  
- you can check your simple webserver by visiting 10.56.35.55 on a different machine  
- alternatively, run "apt install -y curl" and then "curl 10.56.35.55"  
  
## Install, harden/configure, and enable SSH server  
SSH is the best way to securely manage servers remotely.  
- apt install ssh openssh-server #install SSH and SSH server  
- using your preferred text editor (for example nano, vi, vim, emacs, etc.) edit /etc/ssh/sshd_config  
- add the lines   
```'
Port 3333 ## Change default SSH port 22 to 3333 - not for security but to avoid automated bot scans
PasswordAuthentication no ## Disables password authentication for security, SSH key authentication is more secure
PermitRootLogin no ## Disables root login using SSH for security, SSHing into user then su is more secure
```
- place your [public SSH keys](https://docs.gitlab.com/ee/ssh/#generate-an-ssh-key-pair) into /home/bruger/.ssh/authorized_keys  (an easy way to do it is using curl http://gitlab.com/user.keys >> /home/bruger/.ssh/authorized_keys)   
- systemctl enable ssh  #enable ssh server (sshd which means ssh daemon is symlinked to ssh) using systemctl that handles systemd services   
- systemctl start ssh #start ssh server right now using systemctl that handles systemd services  
- access your machine using "ssh bruger@10.56.35.55 -p3333"  
  
## Set firewall rules  
There are two approaches:  
  
### Using Iptables  
- echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc && source /root/.bashrc # add /usr/sbin/ to the PATH environmental variable so the system knows where to execute iptables from  
- iptables -t filter -A INPUT -i lo -j ACCEPT # allow loopback traffic (good practice, it is from your own machine)   
- iptables -t filter -A OUTPUT -o lo -j ACCEPT # allow loopback traffic (good practice, it is from your own machine)   
- iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT ## to make it work with established connections  
- iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT ## to make it work wit hestablishd connections  
- iptables -A INPUT -p tcp --dport 3333 -j ACCEPT # allow port 3333 (our SSH port) incoming traffic  
- iptables -A OUTPUT -p tcp --dport 3333 -j ACCEPT # allow port 3333 (our SSH port) outgoing traffic   
- iptables -A INPUT -p tcp --dport 80 -j ACCEPT # allow port 80 (default http port, our web server) incoming traffic    
- iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT # allow port 80 (default http port, our web server) outgoing traffic   
- iptables -t filter -P INPUT DROP  # drop input packets   
- iptables -t filter -P FORWARD DROP # drop forward packets    
- iptables -t filter -P OUTPUT DROP # drop output packets  
- iptables-save > /etc/iptables/rules.v4 # save configuration      
- ip6tables-save > /etc/iptables/rules.v6 # save ipv6 configuration   
- to restore rules after reboot, add the following line to your cron (crontab -e):  
```
@reboot /sbin/iptables-restore < /etc/iptables/rules.v4
@reboot /sbin/ip6tables-restore < /etc/iptables/rules.v6

```

### Using UFW (Uncomplicated FireWall - Iptables frontend/wrapper)  
- apt install ufw  #install ufw   
- echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc && source /root/.bashrc # add /usr/sbin/ to the PATH environmental variable so the system knows where to execute ufw from  
- ufw allow 3333  # allow port 3333 (our SSH port) incoming traffic  
- ufw allow 80    # allow port 80 (default http port, our web server) incoming traffic  
- sudo ufw default deny incoming # deny incoming connections by default   
- ufw enable  # enable ufw iptables frontend firewall  
