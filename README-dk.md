# Repository for IT Sikkerheds uddannelsen 2021 Baseline Projekt Gruppe 3  

[English/Engelsk](https://its2021team3a.gitlab.io/its_2021_baseline_grp3/)

## Gruppe medlemmer:  
- John Richard Conley
- Levente Atilla Taner  
- Marc Rasmus Jensen  
- Martin Hansson  
- Thomas Tønnes Edelberth    

## Oversigt
Målet med dette projekt er at lave et virtualiseret miljø der består af flere enheder med forskellige oprativ systemer, der emulerer et virkeligt scenarie for bedre at forstå hvordan lignende systemer opsættes fra bunden og hvordan de virker. Dette vil give erfaring med at arbejde med systemer med sikkerhed i fokus og med et miljø til frremtidige tests.  


## Systemet der skal bygges:
<table>
	<tr>
		<th>Enheds navn</th>
		<th>Beskrivelse</th>
		<th>OS</th>
		<th>Disk</th>
		<th>RAM</th>
		<th>CPU</th>
	</tr>
	<tr>
		<td>[Dc1](setup/Dc1.md)</td>
		<td>Primær domain-controller og DHCP</td>
		<td>WS 2016</td>
		<td>20 GB</td>
		<td>4 GB</td>
		<td>2</td>
	</tr>
	<tr>
		<td>[Dc2](setup/Dc2.md)</td>
		<td>Sekundær domain controller</td>
		<td>WS 2016</td>
		<td>20 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[Fil1](setup/Fil1.md)</td>
		<td>Fil server</td>
		<td>WS 2016</td>
		<td>20 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>Backup</td>
		<td>Backup opbevarings server</td>
		<td>WS 2016</td>
		<td>40 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[Sql](setup/Sql.md)</td>
		<td>MS SQL server 2016</td>
		<td>WS 2016</td>
		<td>40 GB</td>
		<td>8 GB</td>
		<td>2</td>
	</tr>
	<tr>
		<td>[Web A](setup/Web-A.md)</td>
		<td>Nginx web server</td>
		<td>Linux</td>
		<td>16 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[Web B](setup/Web-B.md)</td>
		<td>Nginx web server</td>
		<td>Linux</td>
		<td>16 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[Win1](setup/Win1.md)</td>
		<td>Domæne PC</td>
		<td>Windows 10</td>
		<td>20 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[Win2](setup/Win2.md)</td>
		<td>Domæne PC</td>
		<td>Windows 10</td>
		<td>20 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[Kali](setup/Kali.md)</td>
		<td>Værktøj</td>
		<td>Kali Linux</td>
		<td>16 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[LibreNMS](setup/LibreNMS.md)</td>
		<td>Netværksovervågning SNMP</td>
		<td>Linux</td>
		<td>16 GB</td>
		<td>4 GB</td>
		<td>1</td>
	</tr>
	<tr>
		<td>[vSRX](setup/vSRX.md)</td>
		<td>Router/firewall</td>
		<td>WS 2016</td>
		<td>16 GB</td>
		<td>4 GB</td>
		<td>2</td>
	</tr>
</table>

Dette miljø vil køre på en server der kører VMWare ESXi.
Server detaljer: 3 Dell servere med 96 GB RAM, 2 Intel (R) Xeon (R) Silver med 8 kerner i hver, 4 netværkskort 1GB og 1 storage connector der er forbundet til storage controllers A og B via 12GB forbindelse til en fælles direct storage som gør vmotion muligt.


## IP adresser
Gruppen er blevet tildelt VLAN103 og IP rækkevidden 10.56.35.1/24
Desuden er netværket opdelt i fire segmenter:  
DMZ:  
.0 -> .63

Servere:  
.64 -> .127  

Tools:  
.128 -> .191  

DHCP:  
.192 -> .255  

## High Level Diagram  

![diagram](diagram.png)



